import {Button, Grid, makeStyles, Paper, TextField} from "@material-ui/core";
import {useState, useContext} from 'react'
import Header from './components/Header/Header'
import ScrollToTop from './components/ScrollToTop/ScrollToTop'
import Footer from './components/Footer/Footer'
import './App.css'
import {ThemeContext} from './contexts/theme'

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "transparent",
        '&:hover': {
            backgroundColor: 'transparent',
        },
    }
}));

const App = () => {
    const classes = useStyles();
    const [{themeName}] = useContext(ThemeContext);
    const [value, setValue] = useState("");
    const [isError, setIsError] = useState(false);
    const [randomValues, setRandomValues] = useState([]);
    const valueChanged = (newValue) => {
        setValue(newValue)
        if (newValue !== "" && !Number.isInteger(parseInt(newValue, 10))) {
            setIsError(true);
            return;
        }
        setIsError(false);
    }

    const generateClicked = async () => {
        const requestOptions = {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                "accept": "application/json",
            }
        };
        const response = await (await fetch(`https://9hfzqgivbk.execute-api.us-east-2.amazonaws.com/default/cloudsAssignment2LambdaFunction?n=${value}`,
            requestOptions)).json();
        const wasError = !Array.isArray(response) || response.some(rv => !Number.isInteger(parseInt(rv, 10)));
        if (wasError) {
            setIsError(true);
            return;
        }
        console.log("RESPONSE", response);
        setRandomValues(response);
    }

    return (
        <div id='top' className={`${themeName} app`}>
            <Header/>
            <main>
                <h2>How many values do you want to generate (between 0 and 9)?</h2>
                <Paper style={{marginTop: "20px", padding: "10px"}}>
                    <Grid container spacing={2} justifyContent="center"
                          alignItems="center">
                        <Grid item xs={12} sm={12} md={6}>
                            <TextField className={classes.root} variant="filled" color="primary"
                                       id="standard-basic"
                                       label="Number"
                                       fullWidth
                                       value={value}
                                       onChange={(event) => valueChanged(event.target.value)}
                                       error={isError}
                                       helperText={isError ? "The input value must be a valid integer number" : ""}
                            />
                        </Grid>
                        <Grid item xs={12} sm={12} md={6}>
                            <Button
                                onClick={generateClicked}
                                disabled={value === "" || isError} style={{textAlign: "center"}} fullWidth variant="outlined" color="primary">
                                Generate
                            </Button>
                        </Grid>
                    </Grid>
                </Paper>
                <Paper style={{marginTop: "20px", padding: "10px", height: "50vh"}}>
                    <div style={{height: "100%", overflowY: "scroll"}}>
                        {randomValues.length > 0 ? randomValues.map((rv, i) => (
                            // eslint-disable-next-line react/no-array-index-key
                            <p key={`${i} - ${rv}`}>{rv}</p>
                        )) : "No random values have been generated"}
                    </div>
                </Paper>
            </main>

            <ScrollToTop/>
            <Footer/>
        </div>
    )
}

export default App
